<?php
session_start();
include ('../time_table_man/deconnect.php');

$lastitem = $conn->query("select id from chat order by id DESC LIMIT 1");
while ($rows = mysqli_fetch_assoc($lastitem)) {
    echo $last_id = $rows['id'];
}

$getlast_message=$conn->query("select message_from,message,is_read from chat where id ='$last_id';");
while ($rows = mysqli_fetch_assoc($getlast_message)) {
    $last_message_from=$rows['message_from'];
    $last_message=$rows['message'];
    $is_read=$rows['is_read'];
}

?>
<?php

include ('../time_table_man/deconnect.php');

if(isset($_SESSION['is_staff'])){
    $is_staff=$_SESSION['is_staff'];
}else{
    $is_staff=false;
}

if(isset($_POST['submit'])) {
    global $message_from;
    if($is_staff==false){
        $message_from=$_SESSION['college_name'];
    }else{
        $message_from=$_SESSION['staff_name'];
    }
    $message=$_POST['message'];
    $send_data=$conn->query("insert into chat (id,message_from,time,message,is_read) values(null,'$message_from',null,'$message',0 )");
}

?>


    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="refresh" content="40">
        <meta name="author" content="">

        <title>Welcome</title>

        <!-- Bootstrap core CSS -->
        <link href="../time_table_man/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../time_table_man/css/exist_course.css" rel="stylesheet">
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <!-- Custom styles for this template -->
        <script src="../time_table_man/vendor/jquery/jquery.min.js"></script>
        <script src="./push/push.js"></script>
        <script src="./push/push.min.js"></script>
        <script src="./push/serviceWorker.min.js"></script>
        <link href="./send_message.css" type="text/css">
        <script src="./send.js"></script>
        <script src="./sw.js"></script>
    </head>

<body>

<?php
global $message_from;
if($is_read==0 and $last_message_from!=$message_from) {
    echo "<script> function start(){
        
  
        Push.Permission.request();
        var message= '$last_message';
        var message_from='$last_message_from';
        Push.create('You May have new messages');
        Push.create(
        message+' from: '+message_from);
        Push.timeout(2);
        window.setInterval(1000);
        console.log('hey');
    }
    start();
</script>";
}
?>
    <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                <li class="nav-item active">
                    <?php
                    if(isset($_SESSION['is_staff'])){
                       echo "<a class='nav-link' href='../staff_section/view_course.php'>Home <span class='sr-only'></span></a>";
                    }else{
                        echo "<a class='nav-link' href='../time_table_man/tt_man_hom.php'>Home <span class='sr-only'></span></a>";
                    }?>
                </li>
            </ul>
        </div>
    </nav>
    <div class="jumbotron text-center">
        <label class=""> Send alert to your staff</label>
    </div>


    <div class="site-section bg-image2 overlay ">
        <div class="container" >
            <div class="row justify-content-center">
                <div class="col-lg-7 mb-5">
                    <form action="#" method="post" class="p-5 bg-white">
                        <div class="row form-group">

                        <div class="row form-group" id="message-section">
                            <div  id="show" class="container border show-data rounded-lg text-white"style="overflow-y: scroll; height:400px; width: 470px;">
                                <p >
                                    <?php
                                        global $user;
                                        if(isset($_SESSION['is_staff'])){
                                            $user=$_SESSION['staff_name'];
                                        }else{
                                            $user=$_SESSION['college_name'];
                                        }

                                        function show_data()
                                        {
                                            global $user;
                                            include('../staff_section/deconnect.php');
                                            $get_message = $conn->query("select message_from,time,message from chat");

                                            if (mysqli_connect_error()) {
                                                echo "<script> alert('Connect_Error('.mysqli_connect_errno().')'.mysqli_connect_error();); </script>";
                                            } elseif ($get_message) {
                                                while ($rows = mysqli_fetch_array($get_message, MYSQLI_ASSOC)) {

                                                    $time = $rows['time'];
                                                    if ($user == $rows['message_from']) {
                                                        echo "<label class='rounded float-right' style='background-color: green; opacity: 75%'>";
                                                        echo "&nbsp" . date('h:i:a d/m/y', strtotime($time)) . "&nbsp &nbsp &nbsp &nbsp &nbsp" . $rows['message_from'] . "&nbsp";
                                                        echo "</label><br><br>";
                                                        echo "<label class='rounded float-right' style='background-color: green; opacity:75%'>";
                                                        echo "&nbsp" . $rows['message'] . "&nbsp";
                                                        echo "</label><br><br>";
                                                        echo "<label class='float-right'></label>";
                                                    } else {
                                                        echo "<label class='rounded ' style='background-color: grey'>";
                                                        echo "&nbsp" . $rows['message_from'] . "&nbsp &nbsp &nbsp &nbsp &nbsp" . date('h:i:a d/m/y', strtotime($time)) . "&nbsp";
                                                        echo "</label><br>";
                                                        echo "<label class='rounded' style='background-color: grey'>";
                                                        echo "&nbsp" . $rows['message'] . "&nbsp";
                                                        echo "</label><br>";
                                                        echo "<br>";
                                                    }
                                                }
                                            }
                                        }
                                      show_data();
                                    ?>

                                </p>
                                </div>

                                <textarea name="message" id="message" cols="50" rows="1" class="form-control rounded-0 " placeholder="Leave your message here..."></textarea>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12">
                                <input type="submit" name="submit" value="Send Message" class="btn btn-primary mr-2 mb-2">
                                   <li class="nav-item active">
                    <?php
                    if(isset($_SESSION['is_staff'])){
                      echo "<a href='../staff_section/view_course.php' class='btn btn-secondary mr-2 mb-2'>Home</a>";
                    }else{
                        echo "<a href='../time_table_man/tt_man_hom.php' class='btn btn-secondary mr-2 mb-2'>Home</a>";
                    }?>
                </li>
                                
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </body>
    </html>

<script>
    $(document).on('focus','.message',function () {
        <?php echo "typtinggg";?>
    })
</script>

<?php
include ('../time_table_man/deconnect.php');

$update_status=$conn->query("update chat set is_read=1 where id='$last_id' and message_from!='$user'");


?>