<?php 
session_start();
?>
<!DOCTYPE html>
<html>
<title>Login</title>
<head>
       <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link rel="stylesheet" type="text/css" href="./time_table_man/css/login.css">
     <link href="./time_table_man/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
</head>

    <form  method="POST">
    
        <div class="form-row text-center">
             <h1 class="title text-center">Login</h1>
    <div class="login">
        <input type="text" name="username" class="input--style-5" placeholder="Username" required/>
        <input type="password" class="input--style-5" name="password" placeholder="Password" required/>
        <button type="submit"  value="Submit" class="btn btn-primary btn-block btn-large">Let me in.</button>
        </div>
    </form>
</div>
</html>

<?php

include ('./time_table_man/deconnect.php');

if (isset($_POST['username'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];

    $sql = mysqli_query($conn, "SELECT college_name from college where BINARY college_name = '$username' and college_password = '$password'");
    $rows = mysqli_num_rows($sql);

    if ($rows > 0) {
        $_SESSION['username'] = $username;
        $_SESSION['college_name']=$username;
        echo "<script src=\"https://cdn.jsdelivr.net/npm/sweetalert2@9\"></script>
            <script  src=\"https://code.jquery.com/jquery-3.4.1.slim.min.js\"></script>
            <link href=\"//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@3/dark.css\" rel=\"stylesheet\">
            <script src=\"//cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js\"></script>
            <script>
            Swal.fire({
              title: 'Success!',
              text: 'Login successfully',
              icon: 'success',
            }).then(function() {
            window.location = \"./time_table_man/tt_man_hom.php\";
            });
        </script>";
    } else {
        echo "<script src=\"https://cdn.jsdelivr.net/npm/sweetalert2@9\"></script>
            <script  src=\"https://code.jquery.com/jquery-3.4.1.slim.min.js\"></script>
            <link href=\"//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@3/dark.css\" rel=\"stylesheet\">
            <script src=\"//cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js\"></script>
            <script>
            Swal.fire({
              title: 'Error!',
              text: 'Authentication Failure',
              icon: 'error',
            });
        </script>";

    }
}
?>
