<?php
session_start();
if(!isset($_SESSION['username'])==true){
    header('location:./header.php');
}
include('gen_engine.php');

$whole_timetable = $_SESSION['whole_timetable'];

?>

<html xmlns:https="http://www.w3.org/1999/xhtml">
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/timetable.css">

</head>
<body>

<div class="divclass">
<table border="1" cellspacing="0" class="table-dark table-bordered table-hover table" >
   <thead class="thead-light">
    <tr>
        <th colspan="8" class="text-center thead-dark"> Time Table </th>
    </tr>

    <tr class="bg-primary">
        <th> Time </th>
        <th> Monday </th>
        <th> Tuesday </th>
        <th> Wednesday </th>
        <th> Thursday </th>
        <th> Friday </th>
        <th> Saturday </th>
    </tr>
   </thead>

    <tr class="bg-success">
        <td> 10:00-11:00 </td>
        <td> <input name='row1' style='border: hidden ; outline:0;display:inline-block' value='<?php echo $whole_timetable[0];?>'</td>
        <td> <input name='row2' style='border-style:none; outline:0;display:inline-block' value=' <?php echo $whole_timetable[2]?> '</td>
        <td><input name='row3' style='border-style:none; outline:0;display:inline-block' value=' <?php echo $whole_timetable[4];?> '</td>
        <td> <input name='row4' style='border-style:none; outline:0;display:inline-block' value=' <?php echo $whole_timetable[6];?>'</td>
        <td> <input name='row5' style='border-style:none; outline:0;display:inline-block' value='<?php echo $whole_timetable[8];?> '</td>
        <td> <input name='row6'  style='border-style:none; outline:0;display:inline-block' value=' <?php echo $whole_timetable[10];?>' </td>
    </tr>

    <tr class="bg-success">
        <td> 11:00-12:00 </td>
        <td> <input name='row7' style='border-style:none; outline:0;display:inline-block' value='<?php echo $whole_timetable[1];?>'</td>
        <td>  <input name='row8' style='border-style:none; outline:0;display:inline-block' value='<?php echo $whole_timetable[3]?> '</td>
        <td> <input name='row9' style='border-style:none ;outline:0;display:inline-block' value='<?php echo $whole_timetable[5];?> '</td>
        <td> <input name='row10' style='border-style:none; outline:0;display:inline-block' value='<?php echo $whole_timetable[7];?>'</td>
        <td> <input name='row11' style='border-style:none; outline:0;display:inline-block' value='<?php echo $whole_timetable[9];?> '</td>
        <td> <input name='row12' style='border-style:none; outline:0;display:inline-block' value='<?php echo $whole_timetable[11];?> '</td>

    </tr>

    <tr>
        <th colspan="8" class="text-center"> LUNCH BREAK </th>
    </tr>


    <tr class="bg-success">
        <td>12:45-1:45 </td>
        <td> <input name='row13' style='border-style:none; outline:0;display:inline-block' value='<?php echo $whole_timetable[12];?>'</td>
        <td> <input name='row13' style='border-style:none; outline:0;display:inline-block' value=' <?php echo $whole_timetable[14]?> '</td>
        <td> <input name='row14' style='border-style:none; outline:0;display:inline-block' value='<?php echo $whole_timetable[16];?> '</td>
        <td> <input name='row15' style='border-style:none; outline:0;display:inline-block' value='<?php echo $whole_timetable[18];?>'</td>
        <td> <input name='row16' style='border-style:none; outline:0;display:inline-block' value='<?php echo $whole_timetable[20];?> '</td>
        <td> <input name='row17' style='border-style:none; outline:0;display:inline-block' value='<?php echo $whole_timetable[22];?> '</td>

    </tr>

    <tr class="bg-success">
        <td> 1:45-2:45 </td>
        <td> <input name='row18' style='border-style:none; outline:0;display:inline-block' value='<?php echo $whole_timetable[13];?>'</td>
        <td>  <input name='row19' style='border-style:none; outline:0;display:inline-block' value='<?php echo $whole_timetable[15]?> '</td>
        <td> <input name='row20' style='border-style:none ;outline:0;display:inline-block' value='<?php echo $whole_timetable[17];?> '</td>
        <td> <input name='row21' style='border-style:none; outline:0;display:inline-block' value='<?php echo $whole_timetable[19];?>'</td>
        <td> <input name='row22' style='border-style:none; outline:0;display:inline-block' value='<?php echo $whole_timetable[21];?> '</td>
        <td> <input name='row23' style='border-style:none; outline:0;display:inline-block' value='<?php echo $whole_timetable[23];?> '</td>

    </tr>

    <tr>
        <th colspan="8" class="text-center"> SHORT BREAK </th>
    </tr>

    <tr class="bg-success">
        <td> 3:00-4:00 </td>
        <td> <input name='row24' style='border-style:none; outline:0;display:inline-block' value='<?php echo $whole_timetable[24];?>'</td>
        <td> <input name='row25' style='border-style:none; outline:0;display:inline-block' value='<?php echo $whole_timetable[26]?> '</td>
        <td> <input name='row26' style='border-style:none; outline:0;display:inline-block' value='<?php echo $whole_timetable[28];?> '</td>
        <td> <input name='row27' style='border-style:none; outline:0;display:inline-block' value='<?php echo $whole_timetable[30];?>'</td>
        <td> <input name='row28' style='border-style:none; outline:0;display:inline-block' value='<?php echo $whole_timetable[32];?> '</td>
        <td> <input name='row29' style='border-style:none; outline:0;display:inline-block' value='<?php echo $whole_timetable[34];?> '</td>

    </tr>

    <tr class="bg-success">
        <td> 4:00-5:00 </td>
        <td> <input name='row30' style='border-style:none; outline:0;display:inline-block' value='<?php echo $whole_timetable[25];?>'</td>
        <td> <input name='row31' style='border-style:none; outline:0;display:inline-block' value='<?php echo $whole_timetable[27]?> '</td>
        <td> <input name='row32' style='border-style:none; outline:0;display:inline-block' value='<?php echo $whole_timetable[29];?> '</td>
        <td> <input name='row33' style='border-style:none; outline:0;display:inline-block' value='<?php echo $whole_timetable[31];?>'</td>
        <td> <input name='row35' style='border-style:none; outline:0;display:inline-block' value='<?php echo $whole_timetable[33];?> '</td>
        <td> <input name='row36' style='border-style:none; outline:0;display:inline-block' value='<?php echo $whole_timetable[35];?>' </td>
    </tr>

</table>
    <div class="col-md-12 text-center">
        <button name="go-home" onclick="return warning();" class="btn btn-primary btn-danger">Home</button>
        <button name="refresh"  onclick="return RefreshWindow();" class="btn btn-secondary">Not satisfied with timetable? click here</button>
        <button name="save" onclick="location.href='./json/store_json.php'" class="btn btn-primary">Save Timetable</button>
    </div>
</div>
<br>
</body>
</html>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script  src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
<link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@3/dark.css" rel="stylesheet">
<script src="//cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js"></script>
<script>
    function RefreshWindow()
    {
        window.location.reload(true);
    }
        var inputs=document.getElementsByTagName("input");
        for(var i=0;i<inputs.length;i++){
            inputs[i].disabled="true";
        }

    function warning() {
    setTimeout(function () {

        Swal.fire({
            title: 'Are you sure?',
            text: "to go Home",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'I am sure!'
        }).then((result) => {
            if (result.value) {
                window.location="./header.php";
            }
        })
    },1000);
    }
</script>