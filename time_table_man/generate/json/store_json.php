<?php
session_start();
if(!$_SESSION['username']==true){
    header('location:../college_login.php');
}

try{
    function store_as_json()
    {
    $whole_timetable = $_SESSION['whole_timetable'];
    $monday = $tuesday = $wednesday = $thursday = $friday = $saturday = [];

        $monday =array($whole_timetable[0],$whole_timetable[1],$whole_timetable[12],$whole_timetable[13],$whole_timetable[24],$whole_timetable[25]);
        $tuesday=array($whole_timetable[2],$whole_timetable[3],$whole_timetable[14],$whole_timetable[15],$whole_timetable[26],$whole_timetable[27]);
        $wednesday=array($whole_timetable[4],$whole_timetable[5],$whole_timetable[16],$whole_timetable[17],$whole_timetable[28],$whole_timetable[29]);
        $thursday=array($whole_timetable[6],$whole_timetable[7],$whole_timetable[18],$whole_timetable[19],$whole_timetable[30],$whole_timetable[31]);
        $friday=array($whole_timetable[8],$whole_timetable[9],$whole_timetable[20],$whole_timetable[21],$whole_timetable[32],$whole_timetable[33]);
        $saturday=array($whole_timetable[10],$whole_timetable[11],$whole_timetable[22],$whole_timetable[23],$whole_timetable[34],$whole_timetable[35]);
        $timetable=array("monday"=>$monday,"tuesday"=>$tuesday,"wednesday"=>$wednesday,"thursday"=>$thursday,"friday"=>$friday,"saturday"=>$saturday);
        return json_encode($timetable, JSON_FORCE_OBJECT);
    }
    $course_id=$_SESSION['course_id'];
    $college_name=$_SESSION['college_name'];
    $filename=$course_id.$college_name.'.json';
    if(file_put_contents($filename,store_as_json())) {

        echo "<script src=\"https://cdn.jsdelivr.net/npm/sweetalert2@9\"></script>
            <script  src=\"https://code.jquery.com/jquery-3.4.1.slim.min.js\"></script>
            <link href=\"//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@3/dark.css\" rel=\"stylesheet\">
            <script src=\"//cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js\"></script>
            <script> setTimeout(function () {
            Swal.fire({
              title: 'Saved!',
              text: 'Redirecting to home...',
              icon: 'success',
            }).then(function() {
            window.location = \"../header.php\";
            
            });
            },1000);
        </script>";

    }
}catch (Exception $e){
    echo "<script src=\"https://cdn.jsdelivr.net/npm/sweetalert2@9\"></script>
            <script  src=\"https://code.jquery.com/jquery-3.4.1.slim.min.js\"></script>
            <link href=\"//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@3/dark.css\" rel=\"stylesheet\">
            <script src=\"//cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js\"></script>
            <script>
            setTimeout(function () {
            Swal.fire({
              title: 'error!',
              text: 'Message: '.$e->getMessage();
              icon: 'error',
            }).then(function() {
            window.location = \"../header.php\";
            });
            },1000);
        </script>";
}


