<?php

    session_start();
    include('deconnect.php');
    include('datatest.php');
    $sql = "SELECT course_name FROM course";
    $result = $conn->query($sql);
    $coursename=[];
    if($result->num_rows>0){
        while ($row=$result->fetch_assoc()){
            $coursename[]=$row["course_name"];
        }
    }
    global $subjects;
    global $days;
    global $sem;

    $subjects = $_SESSION['subjects'];
    $days = $_SESSION['days'];
    $sem = $_SESSION['semester'];
    global $pr_subjects;
    global $ref_days;

    $_SESSION['ref_days'] = $ref_days;
    $ref_days=array("monday","tuesday","wednesday","thursday","friday","saturday");

    global $all_slots;
    $pr_subjects = $_SESSION['pr_subjects'];
    $slot1 [0]=$slot2 [0] =$slot3 [0]= $slot4[0] = $slot5[0] = $slot6[0] = $slot7[0] = $slot8[0] = $slot9[0] = $slot10[0] = $slot11[0] = $slot12[0] = $slot13[0] = $slot14[0]= $slot15[0] = $slot16[0] = $slot17[0] = $slot18[0] = "off";
    $slot1 [1]=$slot2 [1] =$slot3 [1]= $slot4[1] = $slot5[1] = $slot6[1] = $slot7[1] = $slot8[1] = $slot9[1] = $slot10[1] = $slot11[1] = $slot12[1] = $slot13[1] = $slot14[1]= $slot15[1] = $slot16[1] = $slot17[1] = $slot18[1] = "off";

    $slot_array1 = array("monday"=>$slot1, "tuesday"=>$slot2, "wednesday"=>$slot3, "thursday"=>$slot4, "friday"=>$slot5, "saturday"=>$slot6);
    $slot_array2 = array("monday"=>$slot7, "tuesday"=>$slot8, "wednesday"=>$slot9, "thursday"=>$slot10, "friday"=>$slot11, "saturday"=>$slot12);
    $slot_array3 = array("monday"=>$slot13, "tuesday"=>$slot14, "wednesday"=>$slot15, "thursday"=>$slot16, "friday"=>$slot17, "saturday"=>$slot18);

    $all_slots = array($slot_array1, $slot_array2, $slot_array3);


try {
    function if_not_exists()
    {

        include('datatest.php');
        global $ref_days;
        $subjects = $_SESSION['subjects'];
        $days = $_SESSION['days'];
        $_SESSION['ref_days'] = $ref_days;
        global $all_slots;
        $keys = null;


        while ($subjects != null) {
            $last_sub = null;
            if (count($subjects) == 5) {
                $five_subjects=true;
                $last_sub = $subjects[4];
                echo $last_sub;
                unset($subjects[4]);
            }
            for ($j = 0; $j <= sizeof($all_slots) - 1; $j++) {
                if (!sizeof($subjects) == null) {
                    $keys = array_rand($subjects, 2);
                    $subject_temp = array($subjects[$keys[0]], $subjects[$keys[1]]);

                    $key_day1 = array_rand($days, 3);//here 3 random days choosing then allocating lec to them
                    $rand_day1 = [];
                    for ($i = 0; $i <= 2; $i++) {
                        $rand_day1[$i] = $days[$key_day1[$i]];
                    }
                    for ($k = 0; $k <= sizeof($days) - 1; $k++) {

                        if (in_array($days[$k], $rand_day1)) { //here if random days items matches days array's items then following code will work.
                            //i.e. it will place subjects to randday array items.
                            $all_slots[$j][$ref_days[$k]] = $subject_temp;//here is big problem i.e. after each itretion the subjecttemp values goes to only first item of every allslots's array;
                            $key1 = $key3 = $key2 = null;
                        }
                    }
                }
                /* here -- subjects which are placed for slot_array1 from
                        subject_temp and randomize other remaninng subjects and repeat above process.*/

                $iget_it = [];
                for ($i = 0; $i <= sizeof($subject_temp) - 1; $i++) {
                    $iget_it[$i] = array_search($subject_temp[$i], $subjects);
                    unset($subjects[$iget_it[$i]]);//remove alloted subjects from array
                }
            }

            //practical allocation
            global $pr_subjects;
            $pos = [];
            $pr_subjects_count = count($pr_subjects) - 1;
            for ($x = 0; $x <= sizeof($all_slots) - 1; $x++) {
                for ($y = 0; $y <= sizeof($days) - 1; $y++) {
                    if ($pr_subjects_count >= 0) {
                        if ($all_slots[$x][$ref_days[$y]][0] === "off") {
                            $all_slots[$x][$ref_days[$y]][0] = $pr_subjects[$pr_subjects_count];
                            $all_slots[$x][$ref_days[$y]][1] = $pr_subjects[$pr_subjects_count];
                            $pos[] = $y;
                            $pr_subjects_count--;
                        }
                    }
                }
            }
            $priority_subjects = $_SESSION['priority_subjects'];
            $priority_subjects_count = count($priority_subjects) - 1;

            for ($i = 0; $i <= 2; $i++) {
                for ($x = 0; $x <= sizeof($all_slots) - 1; $x++) {
                    for ($y = 0; $y <= sizeof($days) - 1; $y++) {
                        if ($priority_subjects_count >= 0) {
                            if ($all_slots[$x][$ref_days[$y]][0] === "off") {
                                $all_slots[$x][$ref_days[$y]][0] = $priority_subjects[$priority_subjects_count];
                                $all_slots[$x][$ref_days[$y]][1] = $priority_subjects[$priority_subjects_count];
                                $pos[] = $y;
                                $priority_subjects_count--;
                            }
                        }
                    }
                }
            }
            $remain_days=array("wednesday","thursday","friday","saturday");
            $key_day = array_rand($remain_days, 3);//here 3 random days choosing then allocating lec to them
            $rand_day = [];
            for ($i = 0; $i <= 2; $i++) {
                $rand_day[$i] = $remain_days[$key_day[$i]];
            }
            for ($x = 0; $x <= 2; $x++) {
                for ($y = 0; $y <= sizeof($remain_days) - 1; $y++) {
                    if (in_array($remain_days[$y], $rand_day)) {
                        if ($all_slots[$x][$remain_days[$y]][0] === "off") {
                            $all_slots[$x][$remain_days[$y]][0] = $last_sub;
                            $pos[] = $y;
                        }
                    }
                }
            }

            $_SESSION['pos'] = $pos;
            global $whole_timetable;
            $whole_timetable = [];

            for ($x = 0; $x <= sizeof($all_slots) - 1; $x++) {
                for ($y = 0; $y <= sizeof($days) - 1; $y++) {
                    for ($z = 0; $z <= 1; $z++) {
                        $whole_timetable[] = $all_slots[$x][$ref_days[$y]][$z];
                    }
                    $whole_timetable;
                    $_SESSION['whole_timetable'] = $whole_timetable;
                }
            }
        }
    }
}catch (Exception $e){
    'Message: '.$e->getMessage();
}

try {
    function if_course_exists()
    {
        include('datatest.php');
        global $subjects;
        global $days;
        global $sem;
        global $all_slots;
        $keys = null;
        global $ref_days;
        $ref_days = array("monday", "tuesday", "wednesday", "thursday", "friday", "saturday");
        $allcourse_id=$_SESSION['allcourse_id'];
        $course_id=$_SESSION['course_id'];
        $college_name=$_SESSION['college_name'];

        $value=$allcourse_id[!array_search($course_id,$allcourse_id)];//here taking other course's id for getting json name.

        $data = file_get_contents("json/".$value.$college_name . '.json');
        $data = json_decode($data, true);

        $new_slotarray1 = array("monday" => array($data["monday"][0], $data["monday"][1]), "tuesday" => array($data["tuesday"][0], $data["tuesday"][1]), "wednesday" => array($data["wednesday"][0], $data["wednesday"][1]), "thursday" => array($data["thursday"][0], $data["thursday"][1]), "friday" => array($data["friday"][0], $data["friday"][1]), "saturday" => array($data["saturday"][0], $data["saturday"][1]));
        $new_slotarray2 = array("monday" => array($data["monday"][2], $data["monday"][3]), "tuesday" => array($data["tuesday"][2], $data["tuesday"][3]), "wednesday" => array($data["wednesday"][2], $data["wednesday"][3]), "thursday" => array($data["thursday"][2], $data["thursday"][3]), "friday" => array($data["friday"][2], $data["friday"][3]), "saturday" => array($data["saturday"][2], $data["saturday"][3]));
        $new_slotarray3 = array("monday" => array($data["monday"][4], $data["monday"][5]), "tuesday" => array($data["tuesday"][4], $data["tuesday"][5]), "wednesday" => array($data["wednesday"][4], $data["wednesday"][5]), "thursday" => array($data["thursday"][4], $data["thursday"][5]), "friday" => array($data["friday"][4], $data["friday"][5]), "saturday" => array($data["saturday"][4], $data["saturday"][5]));

        global $new_all_slots;
        $new_all_slots = array($new_slotarray1, $new_slotarray2, $new_slotarray3);

        while ($subjects != null) {

            $last_sub = null;
            if (count($subjects) == 5) {
                $five_subjects=true;
                $last_sub = $subjects[4];
                unset($subjects[4]);
            }
            for ($j = 0; $j <= sizeof($all_slots) - 1; $j++) {
                if (!sizeof($subjects) == null) {
                    $keys = array_rand($subjects, 2);
                    $subject_temp = array($subjects[$keys[0]], $subjects[$keys[1]]);

                    $key_day1 = array_rand($days, 3);
                    $rand_day1 = [];
                    for ($i = 0; $i <= 2; $i++) {
                        $rand_day1[$i] = $days[$key_day1[$i]];
                    }
                    for ($k = 0; $k <= sizeof($days) - 1; $k++) {
                        if (in_array($days[$k], $rand_day1)) { //here if random days items matches days array's items then following code will work.
                            //i.e. it will place subjects to randday array items.
                            $all_slots[$j][$ref_days[$k]] = $subject_temp;
                            $key1 = $key3 = $key2 = null;
                        }
                    }
                }
                /* here -- subjects which are placed for slot_array1 from
                        subject_temp and randomize other remaninng subjects and repeat above process.*/

                $iget_it = [];
                for ($i = 0; $i <= sizeof($subject_temp) - 1; $i++) {
                    $iget_it[$i] = array_search($subject_temp[$i], $subjects);
                    unset($subjects[$iget_it[$i]]);//remove alloted subjects from array
                }
            }

            //practical allocation
            $pr_subjects = $_SESSION['pr_subjects'];
            $practical=[];
            $pos=[];
            $pr_subjects_count = count($pr_subjects) - 1;
            for ($x = 0; $x <= sizeof($all_slots) - 1; $x++) {
                for ($y = 0; $y <= sizeof($days) - 1; $y++) {
                    if ($pr_subjects_count >= 0) {
                        if ($all_slots[$x][$ref_days[$y]][0] === "off") {

                            $practical[]=$pr_subjects[$pr_subjects_count];
                            $all_slots[$x][$ref_days[$y]][0] = $pr_subjects[$pr_subjects_count];
                            $all_slots[$x][$ref_days[$y]][1] = $pr_subjects[$pr_subjects_count];
                            $pos[]=$y;
                            $pr_subjects_count--;
                        }
                    }
                }
            }
            //print_r($practical);

            $priority_subjects=$_SESSION['priority_subjects'];
            $priority_subjects_count = count($priority_subjects)-1;

           for($i=0;$i<=2;$i++){
            for ($x = 0; $x <= sizeof($all_slots) - 1; $x++) {
                for ($y = 0; $y <= sizeof($days) - 1; $y++) {
                    if ($priority_subjects_count >= 0) {
                        if ($all_slots[$x][$ref_days[$y]][0] === "off") {
                            $all_slots[$x][$ref_days[$y]][0] = $priority_subjects[$priority_subjects_count];
                            $all_slots[$x][$ref_days[$y]][1] = $priority_subjects[$priority_subjects_count];
                            $pos[]=$y;
                            $priority_subjects_count--;
                        }
                      }
                    }
                }
            }

            $remain_days=array("wednesday","thursday","friday","saturday");
            $key_day = array_rand($remain_days, 3);//here 3 random days choosing then allocating lec to them
            $rand_day = [];
            for ($i = 0; $i <= 2; $i++) {
                $rand_day[$i] = $remain_days[$key_day[$i]];
            }
            for ($x = 0; $x <= 2; $x++) {
                for ($y = 0; $y <= sizeof($remain_days) - 1; $y++) {
                    if (in_array($remain_days[$y], $rand_day)) {
                        if ($all_slots[$x][$remain_days[$y]][0] === "off") {
                            $all_slots[$x][$remain_days[$y]][0] = $last_sub;
                            $pos[] = $y;
                        }
                    }
                }
            }
            compare_sem();
        }
    }
}catch (Exception $e){
    echo 'Mesaage: '.$e->getMessage();
}

try {
    function compare_sem()
    {
        global $all_slots;
        global $days;
        global $subjects;
        global $new_all_slots;
        global $ref_days;
        global $conn;
        $sem=$_SESSION['semester'];
        $college_name=$_SESSION['college_name'];

        $teacher_sql = "SELECT lecturer_name FROM subjects where college_name='$college_name' and semester!='$sem'";///replace semester value////////////////
        $teacher_sql2 = "SELECT lecturer_name FROM subjects where college_name='$college_name' and semester='$sem'";//////////////replace semester value/////////////
        $teacher_result = $conn->query($teacher_sql);
        $teacher_result2 = $conn->query($teacher_sql2);

        if ($teacher_result->num_rows > 0) {
            while ($row = $teacher_result->fetch_assoc()) {
                $teacher_data1[] = $row["lecturer_name"];
            }
        }
        if ($teacher_result2->num_rows > 0) {
            while ($row = $teacher_result2->fetch_assoc()) {
                $teacher_data2[] = $row["lecturer_name"];
            }
        }

        $get_timetable_teachers = [];
        $get_timetable_teachers2 = [];
        global $whole_timetable;

        for ($x = 0; $x <= sizeof($all_slots) - 1; $x++) {
            for ($y = 0; $y <= sizeof($days) - 1; $y++) {
                for ($z = 0; $z <= 1; $z++) {
                    $all_slots[$x][$ref_days[$y]][$z];
                    $whole_timetable[] = $all_slots[$x][$ref_days[$y]][$z];
                }
            }
        }

        for ($x = 0; $x <= 35; $x++) {
            $query1 = "select lecturer_name from subjects where college_name='$college_name' and name='$whole_timetable[$x]' ";
            $timetable_teachers = $conn->query($query1);
            if ($timetable_teachers->num_rows > 0) {
                if ($whole_timetable[$y] !== "off") {
                    while ($row = $timetable_teachers->fetch_assoc()) {
                        $get_timetable_teachers[] = $row["lecturer_name"] . "<br>";
                    }
                }
            }
        }

        $anothersem_timetable = [];
        for ($x = 0; $x <= sizeof($new_all_slots) - 1; $x++) {
            for ($y = 0; $y <= sizeof($days) - 1; $y++) {
                for ($z = 0; $z <= 1; $z++) {
                    $anothersem_timetable[] = $new_all_slots[$x][$ref_days[$y]][$z];
                }
            }
        }

        for ($y = 0; $y <= 35; $y++) {
            $query1 = "select lecturer_name from subjects where college_name='$college_name'and name='$anothersem_timetable[$y]' ";
            $timetable_teachers2 = $conn->query($query1);
            if ($timetable_teachers2->num_rows > 0) {
                if ($whole_timetable[$y] !== "off") {
                    while ($row = $timetable_teachers2->fetch_assoc()) {
                        $get_timetable_teachers2[] = $row["lecturer_name"]."<br>";
                    }
                }
            }
        }
        $sizeof_teachers=sizeof($get_timetable_teachers)-1;
        $sizeof_teachers2=sizeof($get_timetable_teachers2)-1;
        if($sizeof_teachers2>$sizeof_teachers){
            array_splice($get_timetable_teachers2,$sizeof_teachers2,$sizeof_teachers);

        }elseif($sizeof_teachers>$sizeof_teachers2){
            array_splice($get_timetable_teachers,$sizeof_teachers,$sizeof_teachers2);

        }
        global $similar_teachers;
        $similar_teachers = [];
        global $position_data;
        $position_data = [];

        //here checks same lectures from both courses at same time
        for ($y = 0; $y <= sizeof($get_timetable_teachers)-1; $y++) {
            if (($whole_timetable[$y]!=="off") and ($anothersem_timetable[$y]!== "off")) {
                if ($get_timetable_teachers2[$y] == $get_timetable_teachers[$y]) {
                     $position_data[] = $y;
                     $similar_teachers[] = array($get_timetable_teachers[$y]);
                }
            }
        }


        if ($position_data != null) {
            shuffle_lec();
        }
        else{
            $_SESSION['whole_timetable']=$whole_timetable;
        }
    }
}catch (Exception $e){
    echo 'Message: '.$e->getMessage();
}


try {
    function shuffle_lec()
    {
        global $whole_timetable;
        global $similar_teachers;
      //  print_r($similar_teachers);
        $size = sizeof($similar_teachers);
        // here minimizes the similar teachers to 0
        if ($size != 0) {
            header("Refresh:0");
        }
        $_SESSION['whole_timetable'] = $whole_timetable;
    }
}catch (Exception $e){
    echo 'Message: '.$e->getMessage();
}

if(sizeof($allcourse_id)==1){
    if_not_exists();
}elseif (sizeof($allcourse_id)==2) {
    if_course_exists();
}




/*
$value=$allcourse_id[!array_search($course_id,$allcourse_id)];//here taking other course's id for getting json name.
global $pos_data;
$pos_data = file_get_contents("json/".$value.'_'.$college_name .'_pos'. '.json');
$pos_data = json_decode($pos_data, true);
echo "cd";
print_r($pos_data);





$size2 =sizeof($similar_practicals);


$similar_practicals=[];

for ($y = 0; $y <= sizeof($pos)-1; $y++) {
    if ($pos[$y] == $pos_data[$y]) {
        $similar_practicals[] = array($pos[$y]);
    }
}
echo "cs";
print_r($similar_practicals);



*/





?>












