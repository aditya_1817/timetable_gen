<?php
session_start();

if(!$_SESSION['username']==true){
    header('location:../college_login.php');
}
    $college_name=$_SESSION['college_name'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title><?php echo "Welcome " .$college_name?></title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/simple-sidebar.css" type="text/css" rel="stylesheet">

</head>
<body>

  <div class="d-flex" id="wrapper">
    <!-- Sidebar -->
      <div class="border-right" id="sidebar-wrapper">
          <div class="sidebar-heading">TimeTable Generator</div>
          <div class="list-group list-group-flush">
              <a href="#" class="list-group-item list-group-item-action bg-light">Home</a>
              <a href="./exist_course.php" class="list-group-item list-group-item-action bg-light">View all courses</a>
              <a href="../chat/send_message.php" class="list-group-item list-group-item-action bg-light">Messages</a>
              <a href="./add_course.php" class="list-group-item list-group-item-action bg-light">Generate new Timetable</a>
              
              <a href="./logout.php" class="list-group-item list-group-item-action bg-light">Logout</a>
          </div>
      </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
        <button class="btn btn-dark" id="menu-toggle">Menu</button>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item active">
              <a class="nav-link" href="./tt_man_hom.php">Home <span class="sr-only">(current)</span></a>
            </li>
          
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                More
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
               <a class="dropdown-item" href="./logout.php">Logout</a>
              </div>
            </li>
          </ul>
        </div>
      </nav>


        <div class="site-blocks-cover" style="overflow: hidden;">
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="container-fluid" style="margin-top: 20px">
                        <div class="row mb-4" data-aos="fade-up" data-aos-delay="200">
                             <div class="col-md-7 float-right" style="position: relative;" data-aos="fade-up" data-aos-delay="200">
                                 <img src="./resources/undraw_online_calendar_kvu2.png" alt="Image" class="img-fluid img-absolute ">
                                 </div>
                            <div class="col-sm-5 mr-auto text-center" style="margin-top: 100px" >
                                <h1>Create Your College Timetable in few clicks</h1>
                                <div class="container-fluid text-center" >
                                    <a href="./add_course.php" class=" btn btn-primary mr-2 mb-2">Get Started</a>
                                </div>
                            </div>
                              
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /#page-content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Menu Toggle Script -->
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>

</body>

</html>


