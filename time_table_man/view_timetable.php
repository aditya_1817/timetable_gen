<?php
session_start();
if(!isset($_SESSION['staff_uname']) and !isset($_SESSION['username'])){
echo "<script src=\"https://cdn.jsdelivr.net/npm/sweetalert2@9\"></script>
            <script  src=\"https://code.jquery.com/jquery-3.4.1.slim.min.js\"></script>
            <link href=\"//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@3/dark.css\" rel=\"stylesheet\">
            <script src=\"//cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js\"></script>
            <script>
            Swal.fire({
              title: 'Please Login!',
              text: 'Are You',
                icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#00FF41',
              cancelButtonText: 'Staff',
              confirmButtonText: 'Timetable Manager'
            }).then((result) => {
              if (result.value) {
                Swal.fire(
                  'Redirecting...',
                  'To college login page',
                  'info'
                )
                   window.location= \"../college_login.php\";
              }else{
                  Swal.fire(
                  'Redirecting',
                  'to staff login page',
                  'info'
                )
                window.location= \"../staff_section/staff_login.php\";
              }
            })
        </script>";
    
}

$id=$_GET['id'];
if(isset($_SESSION['staff_uname'])){
$is_staff=$_SESSION['is_staff'];
}
$college_name=$_SESSION['college_name'];
$json=file_get_contents("generate/json/".$id.$college_name.'.json');
$data =  json_decode($json, true);
$array=array("monday","tuesday","wednesday","thursday","friday","saturday");
?>

<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/timetable.css">
</head>
<body>
<table border="1" cellspacing="0" class="table-dark table-bordered table-hover table">
    <thead class="thead-light">
    <tr>
        <th colspan="8" class="text-center thead-dark"> Time Table </th>
    </tr>

    <tr class="bg-primary">
        <th> Time </th>
        <th> Monday </th>
        <th> Tuesday </th>
        <th> Wednesday </th>
        <th> Thursday </th>
        <th> Friday </th>
        <th> Saturday </th>
    </tr>
    </thead>
    <tr class="bg-success">
        <td > 10:00-11:00 </td>
        <?php
        for($i=0;$i<=sizeof($array)-1;$i++){
            echo'<td>' .$data[$array[$i]][0].'</td>';
        }
        ?>
    </tr >
    <tr class="bg-success">
        <td> 11:00-12:00 </td>
        <?php
        for($i=0;$i<=sizeof($array)-1;$i++){
            echo'<td>' .$data[$array[$i]][1].'</td>';
        }
        ?>
    </tr>
    <tr class="bg-success">
        <th colspan="8" class="text-center"> LUNCH BREAK </th>
    </tr>
    <tr class="bg-success">
        <td>12:45-1:45 </td>
        <?php
        for($i=0;$i<=sizeof($array)-1;$i++){
            echo'<td>' .$data[$array[$i]][2].'</td>';
        }
        ?>
    </tr>
    <tr class="bg-success">
        <td> 1:45-2:45 </td>
        <?php
        for($i=0;$i<=sizeof($array)-1;$i++){
            echo'<td>' .$data[$array[$i]][3].'</td>';
        }
        ?>
    </tr>
    <tr class="bg-success">
        <th colspan="8" class="text-center"> SHORT BREAK </th>
    </tr>
    <tr class="bg-success">
        <td> 3:00-4:00 </td>
        <?php
        for($i=0;$i<=sizeof($array)-1;$i++){
            echo'<td >' .$data[$array[$i]][4].'</td>';
        }
        ?>
    </tr>
    <tr class="bg-success">
        <td> 4:00-5:00 </td>
        <?php
        for($i=0;$i<=sizeof($array)-1;$i++){
            echo'<td>' .$data[$array[$i]][5].'</td>';
        }
        ?>
    </tr>
</table>
<div class="col-md-12 text-center">
    <?php
    if(!isset($is_staff)) {
        echo '<button name="go-home" onclick="location.href=\'./tt_man_hom.php\'" class="btn btn-primary btn-danger">Home</button>';
        echo '<button name="back" onclick="location.href=\'./exist_course.php\'" class="btn btn-primary">Back</button>';
    }else{
        echo '<button name="back" onclick="location.href=\'../staff_section/view_course.php\'" class="btn btn-primary">Back</button>';
    }
    ?>

</div>
<br>
</body>
</html>
