<?php
session_start();

if(!isset($_SESSION['username'])==true){
    header('location:../college_login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>welcome</title>

    <!-- Icons font CSS-->
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/main.css" rel="stylesheet" media="all">

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/simple-sidebar.css" rel="stylesheet">
</head>

<body>

<div class="d-flex" id="wrapper">
    <!-- Sidebar -->
    <div class="bg-light border-right" id="sidebar-wrapper">
        <div class="sidebar-heading">TimeTable Generator</div>
        <div class="list-group list-group-flush">
            <a href="./tt_man_hom.php" class="list-group-item list-group-item-action bg-light">Home</a>
            <a href="./exist_course.php" class="list-group-item list-group-item-action bg-light">View all courses</a>
            <a href="#" class="list-group-item list-group-item-action bg-light">View Existing Timetables</a>
            <a href="#" class="list-group-item list-group-item-action bg-light">Generate new Timetable</a>
            <a href="#" class="list-group-item list-group-item-action bg-light">Help</a>
            <a href="./logout.php" class="list-group-item list-group-item-action bg-light">Logout</a>
        </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

        <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
            <button class="btn btn-dark" id="menu-toggle">Menu</button>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Link</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Dropdown
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>

            <div class="page-wrapper bg-gra-03 p-t-45 p-b-50">
                <div class="wrapper wrapper--w790">
                    <div class="card card-5">
                        <div class="card-heading">
                            <h2 class="title">Create new course</h2>
                        </div>
                        <div class="card-body">
                            <form method="POST">
                                <div class="form-row">
                                    <div class="name">Name</div>
                                    <div class="value">
                                        <div class="input-group">
                                            <div class="input-group-desc">
                                                <input class="input--style-5" type="text" name="course_name">
                                                <label class="label--desc">Course name</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="name">Total Students</div>
                                    <div class="value">
                                        <div class="input-group">
                                            <input class="input--style-5" type="number" name="total_students">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="name">Date</div>
                                    <div class="value">
                                        <div class="input-group">
                                            <input class="input--style-5" type="date" name="date">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row m-b-55">
                                    <div class="name">Semester</div>
                                    <div class="value">
                                        <div class="input-group-desc">
                                            <input class="input--style-5" type="number" name="semester">
                                            <label class="label--desc">Semester in no.</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <label class="label">Make sure above information is correct!</label>
                                </div>
                                <div>
                                    <button class="btn-primary"  type="submit">Add course</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Jquery JS-->
            <script src="vendor/jquery/jquery.min.js"></script>
            <!-- Vendor JS-->
            <script src="vendor/select2/select2.min.js"></script>
            <script src="vendor/datepicker/moment.min.js"></script>
            <script src="vendor/datepicker/daterangepicker.js"></script>

            <!-- Main JS-->
            <script src="js/global.js"></script>
    </div>
    <!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->

<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Menu Toggle Script -->
<script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
</script>
</body>
</html>

<?php
include ('deconnect.php');

echo "<script src=\"https://cdn.jsdelivr.net/npm/sweetalert2@9\"></script>
            <script  src=\"https://code.jquery.com/jquery-3.4.1.slim.min.js\"></script>
            <link href=\"//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@3/dark.css\" rel=\"stylesheet\">
            <script src=\"//cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js\"></script>
            <script>
            Swal.fire({
              title: 'Note',
              text: 'Course name should be like departmentname_yearname',
              icon: 'info',
            })
        </script>";

$college_name=$_SESSION['college_name'];
$check_course=null;
$check_course=$conn->query("select course_name from course where college_name='$college_name'");

if($check_course){
     $row = mysqli_num_rows($check_course);

    if($row<2){
        if(isset($_POST['course_name'])){
            $course_name=$_POST['course_name'];
            $total_students = $_POST['total_students'];
            $sem=$_POST['semester'];
            $date = $_POST['date'];
            $result =null;
            $result;
            $_SESSION['course_name']=$course_name;
            $_SESSION['semester']=$sem;

            $result=$conn->query( "Insert into course (college_name,course_name,total_students,semester,date) values 
    ('$college_name','$course_name','$total_students','$sem','$date')");

            if($result){
                echo "<script src=\"https://cdn.jsdelivr.net/npm/sweetalert2@9\"></script>
            <script  src=\"https://code.jquery.com/jquery-3.4.1.slim.min.js\"></script>
            <link href=\"//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@3/dark.css\" rel=\"stylesheet\">
            <script src=\"//cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js\"></script>
            <script>
            Swal.fire({
              title: 'Success',
              text: 'Course created Successfully',
              icon: 'success',
            }).then(function() {
                window.location='./add_sub.php'
            });
        </script>";
            }
            if(!$result){
                echo "<script> alert('something went wrong');
        </script>";
            }
        }
    }else{
       
        echo "<script src=\"https://cdn.jsdelivr.net/npm/sweetalert2@9\"></script>
            <script  src=\"https://code.jquery.com/jquery-3.4.1.slim.min.js\"></script>
            <link href=\"//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@3/dark.css\" rel=\"stylesheet\">
            <script src=\"//cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js\"></script>
            <script>
            Swal.fire({
              title: 'Error!',
              text: 'There cannot be more than 2 course if you have to create another one just delete any existing course',
              icon: 'error',
            }).then(function() {
            window.location='./tt_man_hom.php';
            });
        </script>";
    }
}



?>