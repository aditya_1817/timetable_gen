<?php
session_start();
if(!isset($_SESSION['username'])==true){
    header('location:../college_login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Create Subjects</title>

    <!-- Icons font CSS-->
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/main.css" rel="stylesheet" media="all">



    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/simple-sidebar.css" rel="stylesheet">
</head>

<body>

<div class="d-flex" id="wrapper">
    <!-- Sidebar -->
    <div class="bg-light border-right" id="sidebar-wrapper">
        <div class="sidebar-heading">TimeTable Generator</div>
        <div class="list-group list-group-flush">
            <a href="./tt_man_hom.php" class="list-group-item list-group-item-action bg-light">Home</a>
            <a href="./exist_course.php" class="list-group-item list-group-item-action bg-light">View all courses</a>
            <a href="#" class="list-group-item list-group-item-action bg-light">View Existing Timetables</a>
            <a href="#" class="list-group-item list-group-item-action bg-light">Generate new Timetable</a>
            <a href="#" class="list-group-item list-group-item-action bg-light">Help</a>
            <a href="./logout.php" class="list-group-item list-group-item-action bg-light">Logout</a>
        </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

        <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
            <button class="btn btn-dark" id="menu-toggle">Menu</button>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Link</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Dropdown
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#"></a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>

        <div class="page-wrapper bg-gra-03 p-t-45 p-b-50">
            <div class="wrapper wrapper--w790">
                <div class="card card-5">
                    <div class="card-heading">
                        <h2 class="title">Add subjects</h2>
                    </div>
                    <div class="card-body">
                        <form method="POST">
                            <div class="form-row">
                                <div class="name">Name</div>
                                <div class="value">
                                    <div class="input-group">
                                        <div class="input-group-desc">
                                            <input class="input--style-5" type="text" name="subject_name" >
                                            <label class="label--desc">Subject name</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="name">Lecturer name</div>
                                <div class="value">
                                    <div class="input-group">
                                        <input class="input--style-5" type="text" name="lecturer_name" >
                                    </div>
                                </div>
                            </div>
                            <div class="form-row p-t-20">
                                <label class="label label--block">Is this subject have Practical also?</label>
                                <div class="p-t-15">
                                    <label class="radio-container m-r-55">Yes
                                        <input type="radio" value="1" checked="checked" name="is_practical">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="radio-container">No
                                        <input type="radio" value="0" name="is_practical">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-row p-t-20">
                                <label class="label label--block" data-toggle="tooltip" title="This subject will have extra practical">Add priority to this subject?</label>
                                <div class="p-t-15">
                                    <label class="radio-container m-r-55">Yes
                                        <input type="radio" value="1" name="priority">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="radio-container">No
                                        <input type="radio" value="0" checked="checked" name="priority">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-row">
                                <label class="label">Click done after all subjects are entered.</label>
                            </div>
                            <div>
                                <button class="btn-primary btn btn--radius-2 btn--blue"  name="add_sub" type="submit">Add subject</button>
                                <button class="btn-secondary btn btn--radius-2 btn--red"  name="done" type="submit">Done</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Jquery JS-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <!-- Vendor JS-->
        <script src="vendor/select2/select2.min.js"></script>
        <script src="vendor/datepicker/moment.min.js"></script>
        <script src="vendor/datepicker/daterangepicker.js"></script>

        <!-- Main JS-->
        <script src="js/global.js"></script>
    </div>
    <!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->

<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Menu Toggle Script -->
<script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
</script>
</body>
</html>

<?php
    include('deconnect.php');

    $college_name=$_SESSION['college_name'];
    $course_name=$_SESSION['course_name'];
    $get_course_id = $conn->query("select course_id from course where course_name='$course_name'");
    if($get_course_id){
        while($row = $get_course_id->fetch_assoc()) {
            echo $course_id=$row["course_id"];
        }
    }


    if(isset($_POST['done'])) {
    $check_sub = $conn->query("select name from subjects where course_id='$course_id'");
    $sub_rows = mysqli_num_rows($check_sub);
        if ($sub_rows >=4 or $sub_rows <= 6) {
        echo "<script>location.href='./generate/timetable.php';</script>";//later replace datatest to iframe.php;
        }else{
            echo "subject should be 4 or 6"; //here add sweetalert
        }
    }
    if(isset($_POST['add_sub'])) {
        $sub_name = $_POST['subject_name'];
        $is_practical = $_POST['is_practical'];
        $lecturer_name = $_POST['lecturer_name'];
        $semester=$_SESSION['semester'];
        $priority= $_POST['priority'];
        $result;
        $already_subject_exist = null;
        $already_subject_exist = $conn->query("select name from subjects where name ='$sub_name' and course_id='$course_id' ");

        $rows = mysqli_num_rows($already_subject_exist);
        if ($rows>0) {
            echo "<script src=\"https://cdn.jsdelivr.net/npm/sweetalert2@9\"></script>
            <script  src=\"https://code.jquery.com/jquery-3.4.1.slim.min.js\"></script>
            <link href=\"//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@3/dark.css\" rel=\"stylesheet\">
            <script src=\"//cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js\"></script>
            <script>
            Swal.fire({
              title: 'Error',
              text: 'Subject already exist',
              icon: 'error',
            })
        </script>";
        }
        $priority_value = null;
        $priority_value = $conn->query("select priority from subjects where priority=1 and college_name ='$college_name' and course_id='$course_id' ");
        $pr_rows = mysqli_num_rows($priority_value);
        if($priority==1 and $pr_rows==3){

                echo "<script src=\"https://cdn.jsdelivr.net/npm/sweetalert2@9\"></script>
            <script  src=\"https://code.jquery.com/jquery-3.4.1.slim.min.js\"></script>
            <link href=\"//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@3/dark.css\" rel=\"stylesheet\">
            <script src=\"//cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js\"></script>
            <script>
            Swal.fire({
              title: 'Priority full',
              text: 'You can still add subjects without Priority!',
              icon: 'error',
            })
        </script>";

            }else {

                $result = $conn->query("Insert into subjects (sub_id,name,semester,lecturer_name,practical,college_name,course_id,priority) values (null,'$sub_name','$semester','$lecturer_name','$is_practical','$college_name','$course_id','$priority')");
                if (mysqli_connect_error()) {
                    echo "<script> alert('Connect_Error('.mysqli_connect_errno().')'.mysqli_connect_error();) </script>";
                } elseif ($result) {
                    echo "<script src=\"https://cdn.jsdelivr.net/npm/sweetalert2@9\"></script>
            <script  src=\"https://code.jquery.com/jquery-3.4.1.slim.min.js\"></script>
            <link href=\"//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@3/dark.css\" rel=\"stylesheet\">
            <script src=\"//cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js\"></script>
            <script>
            Swal.fire({
              title: 'Success',
              text: 'Subject added Successfully',
              icon: 'success',
            })
        </script>";
                } elseif (!$result) {
                    echo "<script> alert('something went wrong');
            </script>";
                }
            }

    }

?>

