<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="../css/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../time_table_man/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../time_table_man/vendor/font-awesome-4.7/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../css/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../time_table_man/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="../time_table_man/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../css/util.css">
	<link rel="stylesheet" type="text/css" href="../css/main.css">
<!--===============================================================================================-->
</head>
<body>
	<form method="post">
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-l-85 p-r-85 p-t-55 p-b-55">
				<form class="login100-form validate-form flex-sb flex-w">
					<span class="login100-form-title p-b-32">
						Account Login
					</span>

					<span class="txt1 p-b-11">
						Username
					</span>
					<div class="wrap-input100 validate-input m-b-36" data-validate = "Username is required">
						<input class="input100" type="text" name="username" >
						<span class="focus-input100"></span>
					</div>
					
					<span class="txt1 p-b-11">
						Password
					</span>
					<div class="wrap-input100 validate-input m-b-12" data-validate = "Password is required">
						<span class="btn-show-pass">
							<i class="fa fa-eye"></i>
						</span>
						<input class="input100" type="password" name="pass" >
						<span class="focus-input100"></span>
					</div>
					
					<div class="flex-sb-m w-full p-b-48">
						<div class="contact100-form-checkbox">
							<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
							<label class="label-checkbox100" for="ckb1">
								Remember me
							</label>
						</div>

						<div>
							<a href="./register.php" class="txt3">
								Not registered? Register here.
							</a>
						</div>
					</div>

					<div class="container-login100-form-btn">
						<button class="login100-form-btn" type="submit">Login
						</button>
					</div>

				</form>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="../Login_v14/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="../Login_v14/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="../Login_v14/vendor/bootstrap/js/popper.js"></script>
	<script src="../Login_v14/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="../Login_v14/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="../Login_v14/vendor/daterangepicker/moment.min.js"></script>
	<script src="../Login_v14/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="../Login_v14/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="main.js"></script>
    </form>
</body>
</html>
<?php
include ('./deconnect.php');

if (isset($_POST['username'])) {
     $username = $_POST['username'];
     $password = $_POST['pass'];
     $is_staff=true;
     $_SESSION['staff_uname']=$username;
     $_SESSION['staff_name']=$username;
     $_SESSION['is_staff']=$is_staff;
    $sql = mysqli_query($conn, "SELECT username,college_name from staff_section where BINARY username = '$username' and password = '$password'");
    $rows = mysqli_num_rows($sql);

    if ($rows > 0) {
        $_SESSION['user'] = $username;
            while ($rows = mysqli_fetch_array($sql, MYSQLI_ASSOC)) {
                $college_name=$rows['college_name'];
                $college_name=ltrim($college_name);
                $_SESSION['college_name']=$college_name;

            }
        echo "<script src=\"https://cdn.jsdelivr.net/npm/sweetalert2@9\"></script>
            <script  src=\"https://code.jquery.com/jquery-3.4.1.slim.min.js\"></script>
            <link href=\"//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@3/dark.css\" rel=\"stylesheet\">
            <script src=\"//cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js\"></script>
            <script>
            Swal.fire({
              title: 'Success!',
              text: 'Login successfully',
              icon: 'success',
            }).then(function() {
            window.location= \"./view_course.php\";
            });
        </script>";
    } else {
        echo "<script src=\"https://cdn.jsdelivr.net/npm/sweetalert2@9\"></script>
            <script  src=\"https://code.jquery.com/jquery-3.4.1.slim.min.js\"></script>
            <link href=\"//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@3/dark.css\" rel=\"stylesheet\">
            <script src=\"//cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js\"></script>
            <script>
            Swal.fire({
              title: 'Error!',
              text: 'Authentication Failure',
              icon: 'error',
            });
        </script>";
    }
}
?>
