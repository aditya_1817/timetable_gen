<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Login or Register</title>

    <!-- Icons font CSS-->
    <link href="../time_table_man/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="../time_table_man/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="../time_table_man/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="../time_table_man/vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="../time_table_man/css/main.css" rel="stylesheet" media="all">



    <!-- Bootstrap core CSS -->
    <link href="../time_table_man/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/reg.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="../time_table_man/css/simple-sidebar.css" rel="stylesheet">
</head>

<body>

<div class="d-flex" id="wrapper">
    <!-- Page Content -->
    <div id="page-content-wrapper">
        <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                </ul>
            </div>
        </nav>

        <div class="page-wrapper p-t-45 p-b-50">
            <div class="wrapper wrapper--w790">
                <div class="card card-5" >
                    <div class="card-heading">
                        <h2 class="title">Register</h2>
                    </div>
                    <div class="card-body">
                        <form method="POST">
                            <div class="form-row">
                                <div class="name">Username</div>
                                <div class="value">
                                    <div class="input-group">
                                        <div class="input-group-desc">
                                            <input class="input--style-5" type="text" name="username" required>
                                            <label class="label--desc">This will be your username</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="name">Create new Password</div>
                                <div class="value">
                                    <div class="input-group">
                                        <input class="input--style-5" type="password" name="password"  required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="name">Retype Password</div>
                                <div class="value">
                                    <div class="input-group">
                                        <input class="input--style-5" type="password" name="retype_password" required>
                                    </div>
                                </div>
                            </div>
                            <?php

                            include('../time_table_man/deconnect.php');

                            $result = $conn->query("select college_name from college");
                            echo "<html>";
                            echo "<body>";
                            echo "<div class='form-group'>";
                            echo "<select name='college' class='form-control'>";
                            echo '<option value="NULL">--select-college--</option>';
                            while ($row = $result->fetch_assoc()) {
                                //unset($college_name);
                                $college_name = $row['college_name'];
                                echo '<option value="'.$college_name.'">'.$college_name.'</option>';
                            }
                            echo "</select>";
                            echo "</div>";
                            echo "</body>";
                            echo "</html>";
                            if(isset($_POST['college'])){
                                if($_POST['college']=='NULL'){
                                    echo "<script src=\"https://cdn.jsdelivr.net/npm/sweetalert2@9\"></script>
                                    <script  src=\"https://code.jquery.com/jquery-3.4.1.slim.min.js\"></script>
                                    <link href=\"//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@3/dark.css\" rel=\"stylesheet\">
                                    <script src=\"//cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js\"></script>
                                    <script>
                                    Swal.fire({
                                      title: 'Warning',
                                      text: 'Please select college',
                                      icon: 'error',
                                    })
                                </script>";
                                }
                            }
                            ?>

                            <div>
                                <button name="submit" class="btn btn-primary btn--radius-2" type="submit">Register</button>
                                <button class="btn btn-primary btn-dark btn--radius-2" onclick=window.location="./staff_login.php">already registered ?</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Jquery JS-->
        <script src="./time_table_man/vendor/jquery/jquery.min.js"></script>
        <!-- Vendor JS-->
        <script src="./time_table_man/vendor/select2/select2.min.js"></script>
        <script src="./time_table_man/vendor/datepicker/moment.min.js"></script>
        <script src="./time_table_man/vendor/datepicker/daterangepicker.js"></script>

        <!-- Main JS-->
        <script src="js/global.js"></script>
    </div>
    <!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->

<!-- Bootstrap core JavaScript -->
<script src="../time_table_man/vendor/jquery/jquery.min.js"></script>
<script src="../time_table_man/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Menu Toggle Script -->
<script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
</script>
</body>
</html>


<?php
if(isset($_POST['username'])) {
    $college_name=$_POST['college'];
    $username=$_POST['username'];
    $password=$_POST['retype_password'];
    echo $q="Insert into staff_section(id,college_name,username,password) values (null,'$college_name','$username','$password')";
    $result = $conn->query("Insert into staff_section(id,college_name,username,password) values (null,'$college_name','$username','$password')");
    if (mysqli_connect_error()) {
      
        echo "<script> alert('Connect_Error('.mysqli_connect_errno().')'.mysqli_connect_error();) </script>";
    } elseif ($result) {

        echo "<script src=\"https://cdn.jsdelivr.net/npm/sweetalert2@9\"></script>
            <script  src=\"https://code.jquery.com/jquery-3.4.1.slim.min.js\"></script>
            <link href=\"//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@3/dark.css\" rel=\"stylesheet\">
            <script src=\"//cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js\"></script>
            <script>
            Swal.fire({
              title: 'Success',
              text: 'Registered',
              icon: 'success',
            }).then(function() {
            window.location = './staff_login.php';
            });
        </script>";
    } elseif (!$result) {
        echo "<script> alert('something went wrong');
            </script>";
    }
}
?>

