<?php
session_start();
if(!$_SESSION['staff_uname']==true){
    header('location:../staff_login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Welcome</title>

    <!-- Bootstrap core CSS -->
    <link href="../time_table_man/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../time_table_man/css/exist_course.css" rel="stylesheet">
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!-- Custom styles for this template -->
</head>

<body>
<div class="d-flex" id="wrapper">
    <!-- Sidebar -->
    <div class="bg-light border-right" id="sidebar-wrapper">
        <div class="sidebar-heading">TimeTable Generator</div>
        <div class="list-group list-group-flush">
            <a href="./logout.php" class="list-group-item list-group-item-action bg-light">Logout</a>
                <a href="../chat/send_message.php" class="list-group-item list-group-item-action bg-light">Messages</a>
        </div>
    </div>
    <!-- Page Content -->
    <div id="page-content-wrapper">
        <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
            <button class="btn btn-dark" id="menu-toggle">Menu</button>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                    <li class="nav-item active">
                        <a class="nav-link" href="./view_course.php">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./logout.php">Logout</a>
                    </li>

                    </li>
                </ul>
            </div>
        </nav>
        <div class="container">
            <form method="POST">
            <div class="row col-md-6 col-md-offset-2 custyle">
                <table class="table table-striped custab table-responsive" >

                    <?php
                    include("./deconnect.php");
                     $college_name=$_SESSION['college_name'];

                    $result = $conn->query("select course_id,course_name,total_students,semester,date from course where college_name='$college_name'");
                     $result;
                    $i = 0;

                    if(mysqli_connect_error()){
                        echo "<script> alert('Connect_Error('.mysqli_connect_errno().')'.mysqli_connect_error();); </script>";
                    }
                    elseif($result)
                    {
                        echo " <thead>
                    <tr class='bg-dark text-white'>
                        <th>id</th>
                        <th>Course Name</th>
                        <th>Total students</th>
                        <th>Semester</th>
                        <th class=\"text-md-center\">Creation date</th>
                        <th class=\"text-center\">Action</th>
                    </tr>
                    </thead>";
                            //display the data
                            while ($rows = mysqli_fetch_array($result,MYSQLI_ASSOC))
                            {
                                echo "<tr>";
                                foreach ($rows as $data)
                                {
                                    echo "<td align='center'>". $data . "</td>";
                                }
                                $id=$rows['course_id'];
                                echo " <td class=\"text-center\"> 
                                        <a href='../time_table_man/view_timetable.php?id=$id' class=\"btn btn-primary btn-xs\"><span class=\"glyphicon glyphicon-remove\"></span> View</a></td>";

                            }
                        }else{
                            echo "<tr><td colspan='" . ($i+1) . "'>No Results found!</td></tr>";
                        }

                    ?>
                    </table>
                </div>
            </form>
    </div>
    <!-- /#page-content-wrapper -->
</div>
<!-- Bootstrap core JavaScript -->
<script src="../time_table_man/vendor/jquery/jquery.min.js"></script>
<script src="../time_table_man/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Menu Toggle Script -->
<script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
</script>

</body>
</html>

<?php
include("./deconnect.php");
if(isset($_POST['select'])) {
    $key = $_POST['delete'];
    echo $key;
    $del = $conn->query("delete * from course where course_id='$key'") or die("ohhhh seems like something bad happaned.");



    include ('../time_table_man/Mobile_Detect.php');
    $detect = new Mobile_Detect();
    if ($detect->isMobile()){
        echo "<script src=\"https://cdn.jsdelivr.net/npm/sweetalert2@9\"></script>
                <script  src=\"https://code.jquery.com/jquery-3.4.1.slim.min.js\"></script>
                <link href=\"//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@3/dark.css\" rel=\"stylesheet\">
                <script src=\"//cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js\"></script>
                <script>
                Swal.fire({
                  title: 'Info',
                  text: 'If you are using mobile device then scroll left to do action',
                  icon: 'info',
                });
            </script>";
    }
    else {
    // other content for desktops
    }

}
?>