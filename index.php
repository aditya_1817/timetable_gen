<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Welcome</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="./time_table_man/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="./time_table_man/vendor/jquery/jquery-ui.css">
    <link rel="stylesheet" href="./time_table_man/vendor/jquery/owl.carousel.min.css">
    <link rel="stylesheet" href="/time_table_man/vendor/jquery/owl.theme.default.min.css">
    <link rel="stylesheet" href="/time_table_man/vendor/jquery/owl.theme.default.min.css">

    <link rel="stylesheet" href="/time_table_man/vendor/jquery/jquery.fancybox.min.css">

    <link rel="stylesheet" href="/time_table_man/vendor/bootstrap/css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="./time_table_man/vendor/mdi-font/fonts/fonts/flaticon/font/flaticon.css">

    <link rel="stylesheet" href="./time_table_man/vendor/bootstrap/css/aos.css">

    <link rel="stylesheet" href="./time_table_man/vendor/jquery/style.css">
    
  </head>
  <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">
  

  <div id="overlayer"></div>
  <div class="loader">
    <div class="spinner-border text-primary" role="status">
      <span class="sr-only">Loading...</span>
    </div>
  </div>

  <div class="site-wrap"  id="home-section">

    <div class="site-mobile-menu site-navbar-target">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>
   
   
    <header class="site-navbar py-4 js-sticky-header site-navbar-target" role="banner">

      <div class="container">
        <div class="row align-items-center">
          
          <div class="col-6 col-md-3 col-xl-4  d-block">
            <h1 class="mb-0 site-logo"><a href="index.php" class="text-black h2 mb-0">Timetable Generator<span class="text-primary">.</span> </a></h1>
          </div>

          <div class="col-12 col-md-9 col-xl-8 main-menu">
            <nav class="site-navigation position-relative text-right" role="navigation">

              <ul class="site-menu main-menu js-clone-nav mr-auto d-none d-lg-block ml-0 pl-0">
                <li><a href="#home-section" class="nav-link">Home</a></li>
                <li><a href="#features-section" class="nav-link">Features</a></li>
                <li class="has-children">
                  <a href="#about-section" class="nav-link">About Us</a>
                  <ul class="dropdown arrow-top">
                    <li><a href="#our-team-section" class="nav-link">Our Team</a></li>
                  </ul>
                </li>
                <li><a href="#help-section" class="nav-link">Help</a></li>
              </ul>
            </nav>
          </div>
          <div class="col-6 col-md-9 d-inline-block d-lg-none ml-md-0" ><a href="#" class="site-menu-toggle js-menu-toggle text-black float-right"><span class="icon-menu h3"></span></a></div>
        </div>
      </div>
      
    </header>
    

    <div class="site-blocks-cover" style="overflow: hidden;">
      <div class="container">
        <div class="row align-items-center justify-content-center">

          <div class="col-md-9" style="position: relative;" data-aos="fade-up" data-aos-delay="200">
            
            <img src="./time_table_man/resources/undraw_time_management_30iu.png" alt="Image" class="img-fluid img-absolute ">

            <div class="row mb-4" data-aos="fade-up" data-aos-delay="200">
              <div class="col-lg-6 mr-auto">
                <h1>Manage Your College Timetable in few clicks</h1>
                <div>
                  <a href="./register.php" class="btn btn-primary mr-2 mb-2">Get Started</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>  


    <div class="site-section" id="features-section">
      <div class="container">
        <div class="row mb-5 justify-content-center text-center"  data-aos="fade-up">
          <div class="col-7 text-center  mb-5">
            <h2 class="section-title">Features</h2>
            <p class="lead"></p>
          </div>
        </div>
        <div class="row align-items-stretch">
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up">
            <div class="unit-4 d-block">
              <div class="unit-4-icon mb-3">
                <span class="icon-wrap"><span class="text-primary icon-autorenew"></span></span>
              </div>
              <div>
                <h3>Efficient and user friendly application</h3>
                <p>Provides efficient and user friendly environment.</p>
              </div>
            </div>

          </div>
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="100">

            <div class="unit-4 d-block">
              <div class="unit-4-icon mb-3">
                <span class="icon-wrap"><span class="text-primary icon-store_mall_directory"></span></span>
              </div>
              <div>
                <h3>Quick and Easy </h3>
                <p>Generates timetable easily and within minutes also.</p>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up"  data-aos-delay="200">
            <div class="unit-4 d-block">
              <div class="unit-4-icon mb-3">
                <span class="icon-wrap"><span class="text-primary icon-shopping_basket"></span></span>
              </div>
              <div>
                <h3>Manage staff schedule</h3>
                <p>Manage the schedule of staff on the basis of subjects.</p>
              </div>
            </div>
          </div>


          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up">
            <div class="unit-4 d-block">
              <div class="unit-4-icon mb-3">
                <span class="icon-wrap"><span class="text-primary icon-settings_backup_restore"></span></span>
              </div>
              <div>
                <h3>Free Updates</h3>
                <p>Get Updates without free of cost</p>
              </div>
            </div>
          </div>

          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="100">
            <div class="unit-4 d-block">
              <div class="unit-4-icon mb-3">
                <span class="icon-wrap"><span class="text-primary icon-sentiment_satisfied"></span></span>
              </div>
              <div>
                <h3>Decrease manual work and time</h3>
                <p>Instead of generating time table manually, use this application it will save time and work as well.</p>
              </div>
            </div>

            
          </div>
          
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="200">
            <div class="unit-4 d-block">
              <div class="unit-4-icon mb-3">
                <span class="icon-wrap"><span class="text-primary icon-power"></span></span>
              </div>
              <div>
                <h3>Reduce work load & man power</h3>
                <p>It reduces time, work load and man power. </p>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
    
    <div class="feature-big">
      <div class="container">
        <div class="row mb-5 site-section">
          <div class="col-lg-7" data-aos="fade-right">
            <img src="./time_table_man/resources/undraw_gift_card_6ekc.svg" alt="Image" class="img-fluid">
          </div>
          <div class="col-lg-5 pl-lg-5 ml-auto mt-md-5">
            <h2 class="text-black">Just few Steps away from creating your first time table</h2>
              <p><a href="#help-section" class="btn btn-primary">Learn More</a></p>
          </div>
        </div>

      </div>
    </div>


    <div class="site-section bg-light" id="about-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-12 text-center">
            <h2 class="section-title mb-3">About Us</h2>
          </div>
        </div>
        <div class="row mb-5">
          <div class="col-lg-6" data-aos="fade-right">
            <img src="./time_table_man/resources/undraw_bookmarks_r6up.svg" alt="Image" class="img-fluid">
          </div>
          <div class="col-lg-5 ml-auto pl-lg-5">
            <h2 class="text-black mb-4 h3 font-weight-bold">Our Mission</h2>
            <p class="mb-4" style="color: black; opacity: 70%">Our mission is to satisfy the customer with a proper, accurate, best-fit, efficient and valuable Time Table and also to schedule the staff on the basis of subjects.
            </p>
          </div>
        </div>
      </div>
    </div>

      <div class="site-section bg-light" id="help-section">
      <div class="jumbotron text-center" style="font-size: 20px" >
          Help
      </div>
      <div class="container bg-dark rounded-lg text-white">
          <p class="mb-4">Steps to generate time table -:.<br>
              1. Click on the Button “Get Started”.<br>
              2. Register all the details and click on “REGISTER" button or if u already registered then go with “ALREADY REGISTERD”.<br>
              3. If registered then “LOGIN” with username and password.<br>
              4. Now, to generate your college Timetable in few clicks, click on “Get Started”.<br>
              5. “CREATE NEW COURSE” and add it by following all the credentials.<br>
              6. ADD SUBJECTS, add all the subjects and click “DONE” after adding all the subjects.<br>
              7. And you’re TIMETABLE gets generate.<br>
              8. You can save it with “Save Timetable” button and can view or delete it from “View all course” option present in the “MENU” or “MENU dashboard”.<br> </p>

      </div>
      <div class="col-md-12 text-center">
          <a href='#home-section' class="btn btn-primary text-center">Go top</a>
      </div>

    <div class="footer py-5 text-center">
      <div class="container">

        <div class="row">
          <div class="col-sm-12"style="color: rgba(164,191,160,1)" >
            <p class="small">
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart text-danger" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank" >Colorlib</a>
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            </p>
          </div>
        </div>
      </div>
    </div>
      </div>
  </div> <!-- .site-wrap -->

  <script src="./time_table_man/js/jquery-3.3.1.min.js"></script>
  <script src="./time_table_man/js/jquery-ui.js"></script>
  <script src="./time_table_man/js/popper.min.js"></script>
  <script src="./time_table_man/js/bootstrap.min.js"></script>
  <script src="./time_table_man/js/owl.carousel.min.js"></script>
  <script src="./time_table_man/js/jquery.countdown.min.js"></script>
  <script src="./time_table_man/js/bootstrap-datepicker.min.js"></script>
  <script src="./time_table_man/js/jquery.easing.1.3.js"></script>
  <script src="./time_table_man/js/aos.js"></script>
  <script src="./time_table_man/js/jquery.fancybox.min.js"></script>
  <script src="./time_table_man/js/jquery.sticky.js"></script>

  
  <script src="./time_table_man/js/main.js"></script>
  
  </body>
</html>

